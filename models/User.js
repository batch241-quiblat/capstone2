const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [
		{
			productId: {
				type: String,
				required: [true, "ProductId is required"]
			},
			productName: {
				type: String,
				required: [true, "ProductName is required"]
			},
			productDescription: {
				type: String,
				required: [true, "ProductDescription is required"]
			},
			productCategory: {
				type: String,
				required: [true, "ProductCategory is required"]
			},
			productPrice: {
				type: Number,
				required: [true, "ProductPrice is required"]
			},
			addedOn: {
				type: Date,
				default: new Date()
			},
			quantity: {
				type: Number,
				default: 1
			},
			subTotal:{
				type: Number,
				required: [true, "Product's subTotal is required"]
			}
		}
	],
	totalPriceOfCart:{
		type: Number,
		default: 0
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("User", userSchema)