const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");

let message;

// Add Order
module.exports.createOrder = async(userId, productId, reqBody) => {

	// Find Product
	let product = await Product.findById(productId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}
		else if(result.isAvailable == false){
			return "not available";
		}else {
			return result;
		}	
	});

	if(product == "error") {
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(product == null){
		message = "Product does not exists.";
		return {error: message};
	}
	else if(product == "not available"){
		message = "Product is not available.";
		return {error: message};
	}

	let newOrder = new Order({
		userId: userId,
		products: [
			{
				productId: productId,
				quantity: reqBody.quantity
			}
		],
		totalAmount: product.price * reqBody.quantity
	});

	return newOrder.save().then((result, error) => {
		if(error){
			message = "There is an error with the database";
			return {error: message};
		} else {
			const capitalizedName = product.name[0].toUpperCase() + product.name.slice(1);
			message = `${capitalizedName} is successfully ordered.`
			return {success: message};
		}
	});
}

// Retrieve My Orders
module.exports.getMyOrders = (myId) => {
	return Order.find({userId: myId}).then((result, error) => {
		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "No Orders yet.";
			return {error: message};
		}else {
			return result;
		}
	});
}

// Retrieve All Orders
module.exports.getAllOrders = () => {
	return Order.find().then((result, error) => {
		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "No Orders yet.";
			return {error: message};
		}else {
			return result;
		}
	});
}

// Retrieve Specific Orders
module.exports.getUserOrders = (Id) => {
	return Order.find({userId: Id}).then((result, error) => {
		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "No Orders yet.";
			return {error: message};
		}else {
			return result;
		}
	});
}