const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../middlewares/auth");
const Order = require("../models/Order");

let message;

// Check Email if Exists User
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	});
}

// User Registration
module.exports.registerUser = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0){
			message = "Email is already taken."
			return {error: message};
		} else {
			let newUser = new User({
				firstName: reqBody.firstName.toLowerCase(),
				lastName: reqBody.lastName.toLowerCase(),
				mobileNo: reqBody.mobileNo.toLowerCase(),
				email: reqBody.email.toLowerCase(),
				password: bcrypt.hashSync(reqBody.password, 10)
			});
			return newUser.save().then((user, error) => {
				if(error){
					message = "There is an error with the database.";
					return {error: message};
				} else {
					const capitalizedName = reqBody.firstName[0].toUpperCase() + reqBody.firstName.slice(1);
					message = `Congratulations ${capitalizedName}! You are now successfully registered. Enjoy your shopping with us.`
					return {success: message};
				}
			});
		}
	});
}

// User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null || result == ""){
			message = "User does not exists."
			return {error: message};
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				message = "Wrong Password."
					return {error: message};
			}
		}
	});
}

// User Profile
module.exports.userProfile = (userId) => {
	return User.findById(userId).then((result, error) => {
		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "User does not exists."
			return {error: message};
		}else {
			result.password = "";
			return result;
		}
	});
}

// Update Profile
module.exports.updateProfile = (userId, reqBody) => {
	let profile = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo
	}
	return User.findByIdAndUpdate(userId, profile).then((user, error) => {
		if(error){
			message = "Update failed. There is an error with the database.";
			return {error: message};
		}
		else if(user == null){
			message = "User does not exists."
			return {error: message};
		} else {
			message = "Profile is successfully updated.";
			return {success: message};
		}
	});
}

// Change Password
module.exports.updatePassword = (userId, reqBody) => {

	return User.findById(userId).then(result => {
		console.log(result);
		if(result == null){
			message = "User does not exists.";
			return {error: message};
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.oldPassword, result.password);
			result.password = bcrypt.hashSync(reqBody.newPassword, 10);
			if(isPasswordCorrect){
				return result.save().then((user, error) => {
					if(error){
						message = "Update failed. There is an error with the database.";
						return {error: message};
					} else {
						message = "Password is successfully changed.";
						return {success: message};
					}
				});
			} else {
				message = "Old password is incorrect.";
				return {error: message};
			}
		}
	});
}

// Get All Users
module.exports.displayAllUsers = () => {
	return User.find({isAdmin: false}).then(result => {
		if(result == null || result.length == 0){
			message = "There are no users yet. Please add one.";
			return {error: message};
		} else {
			result.map((user) => user.password = "");
			return result;
		}
	});
}

// Get Specific User
module.exports.displayUser = (userId) => {
	return User.findById(userId).then((result, error) => {	

		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "User does not exists."
			return {error: message};
		}
		else if(result.isAdmin == true){
			message = "Cannot retrieve admin details."
			return {error: message};
		}else {
			result.password = "";
			return result;
		}
	});
}

// Set User As Admin
module.exports.setUserAsAdmin = (userId, reqBody) => {

	let updateStatus = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(userId, updateStatus).then((result, error) => {
		if(error){
			message = "Update failed. There is an error with the database.";
			return {error: message};
		}
		else if(result == null){
			message = "User does not exists."
			return {error: message};
		}
		 else {
		 	const capitalizedName = result.firstName[0].toUpperCase() + result.firstName.slice(1);
			message = `${capitalizedName} is now an admin.`;
			return message;
		}
	});
}

// Add To Cart
module.exports.addToCart = async(userId, productId, reqBody) => {

////////////////////////////////////////////////////////////////////////////////
	// Find Product
	let product = await Product.findById(productId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}
		else if(result.isAvailable == false){
			return "not available";
		}else {
			return result;
		}	
	});

	if(product == "error") {
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(product == null){
		message = "Product does not exists.";
		return {error: message};
	}
	else if(product == "not available"){
		message = "Product is not available.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Add Product To User's Cart
	let userCart = await User.findById(userId).then((user, error) => {
		let items = user.cart;
		let count = 0; 
		for(let i = 0; i < items.length; i++){
			if(items[i].productId == productId){
				count = count + 1;
				break;
			}
		}
		if(error){
			return "error";
		} 
		else if(user == null){
			return null;
		}
		else if(count == 1){
			return "exists";
		}else {
			user.cart.push({
				productId: productId,
				productName: product.name,
				productDescription: product.description,
				productCategory: product.category,
				productPrice: product.price,
				quantity: reqBody.quantity,
				subTotal: product.price * reqBody.quantity
			});
			return user.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return user.cart;
				}
			});	
		}
	});

	if(userCart == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(userCart == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(userCart == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(userCart == "exists"){
		message = "Product is already in the cart.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Compute Total Of Cart
	let totalOfCart = userCart.map((items) => {
		return items.subTotal;
	});
	let totalOfCart2 = 0;
	for(let i = 0; i < totalOfCart.length; i++){
		totalOfCart2 = totalOfCart2 + totalOfCart[i];
	}

	// Update Total Cart Amount
	let isUserCartTotalUpdated = await User.findById(userId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}else {
			
			result.totalPriceOfCart = totalOfCart2;

			return result.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		}	
	});

	if(isUserCartTotalUpdated == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == true){
		const capitalizedName = product.name[0].toUpperCase() + product.name.slice(1);
		message = `${capitalizedName} is added to your cart.`;
		return message;
	}
}

// Update Quantity
module.exports.updateQuantity = async(userId, productId, reqBody) => {

////////////////////////////////////////////////////////////////////////////////
	// Find Product
	let product = await Product.findById(productId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}
		else if(result.isAvailable == false){
			return "not available";
		}else {
			return result;
		}	
	});

	if(product == "error") {
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(product == null){
		message = "Product does not exists.";
		return {error: message};
	}
	else if(product == "not available"){
		message = "Product is not available.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Update Quantity
	let userCart = await User.findById(userId).then((user, error) => {
		if(error){
			return "error";
		} 
		else if(user == null){
			return null;
		}else {
			let items = user.cart;
			let arr = 0; 
			let count = 0;
			for(let i = 0; i < items.length; i++){
				if(items[i].productId == productId){
					user.cart[i].quantity = reqBody.quantity;
					count = count + 1;
					break;
				}
				arr = arr + 1;
			}
			if(count ==  0){
				return "empty";
			}else{
				user.cart[arr].subTotal = product.price * reqBody.quantity;
				return user.save().then((result, error) => {
					if(error){
						return false;
					} else {
						return result.cart;
					}
				});	
			}
			
		}
	});
	

	if(userCart == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(userCart == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(userCart == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(userCart == "empty"){
		message = "Product is not in the cart.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Compute Total Of Cart
	let totalOfCart = userCart.map((items) => {
		return items.subTotal;
	});

	let totalOfCart2 = 0;
	for(let i = 0; i < totalOfCart.length; i++){
		totalOfCart2 = totalOfCart2 + totalOfCart[i];
	}

	// Update Total Cart Amount
	let isUserCartTotalUpdated = await User.findById(userId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}else {
			
			result.totalPriceOfCart = totalOfCart2;

			return result.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		}	
	});

	if(isUserCartTotalUpdated == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == true){
		message = "Quantity is updated.";
		return {error: message};
	}
}


// Remove Product From Cart
module.exports.removeProduct = async(userId, productId) => {

////////////////////////////////////////////////////////////////////////////////
	// Find Product
	let product = await Product.findById(productId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}
		else if(result.isAvailable == false){
			return "not available";
		}else {
			return result;
		}	
	});

	if(product == "error") {
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(product == null){
		message = "Product does not exists.";
		return {error: message};
	}
	else if(product == "not available"){
		message = "Product is not available.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Update Quantity
	let userCart = await User.findById(userId).then((user, error) => {
		if(error){
			return "error";
		} 
		else if(user == null){
			return null;
		}else {
			let items = user.cart;
			let arr = 0; 
			let count = 0;
			for(let i = 0; i < items.length; i++){
				
				if(items[i].productId == productId){
					count = count + 1;
					break;		
				}
				arr = arr + 1;
			}
			
			if(arr > -1){
				items.splice(arr, 1)
			}
			if(count == 0){
				return "empty";
			}
			return user.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return result.cart;
				}
			});	
		}
	});
	console.log(userCart);

	if(userCart == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	// else if(userCart == false){
	// 	message = "User cart is empty.";
	// 	return message;
	// }
	else if(userCart == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(userCart == "empty"){
		message = "Product is not in the cart.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Compute Total Of Cart
	let totalOfCart = userCart.map((items) => {
		return items.subTotal;
	});

	let totalOfCart2 = 0;
	for(let i = 0; i < totalOfCart.length; i++){
		totalOfCart2 = totalOfCart2 + totalOfCart[i];
	}

	// Update Total Cart Amount
	let isUserCartTotalUpdated = await User.findById(userId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}else {
			
			result.totalPriceOfCart = totalOfCart2;

			return result.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		}	
	});

	if(isUserCartTotalUpdated == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == true){
		message = "Product is remove from the cart.";
		return {error: message};
	}
}

// Checkout Product From Cart
module.exports.checkout = async(userId, productId) => {

////////////////////////////////////////////////////////////////////////////////
	// Find Product
	let product = await Product.findById(productId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}
		else if(result.isAvailable == false){
			return "not available";
		}else {
			return result;
		}	
	});

	if(product == "error") {
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(product == null){
		message = "Product does not exists.";
		return {error: message};
	}
	else if(product == "not available"){
		message = "Product is not available.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Check Prodcut In Cart
	let checkoutItem = await User.findById(userId).then((user, error) => {
		if(error){
			return "error";
		} 
		else if(user == null){
			return null;
		}else {
			let items = user.cart;
			let arr = 0; 
			let count = 0;
			for(let i = 0; i < items.length; i++){
				if(items[i].productId == productId){
					count = count + 1;
					break;		
				}
				arr = arr + 1;

			}
			if(count ==  0){
				return "empty";
			}
			return user.cart[arr];
		}
	});

	let newOrder = new Order({
		userId: userId,
		products: [
			{
				productId: checkoutItem.productId,
				quantity: checkoutItem.quantity
			}
		],
		totalAmount: checkoutItem.subTotal
	});

	console.log(checkoutItem);
	console.log(newOrder);

	if(checkoutItem == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	// else if(userCart == false){
	// 	message = "User cart is empty.";
	// 	return message;
	// }
	else if(checkoutItem == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(checkoutItem == "empty"){
		message = "Product is not in the cart.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Update Quantity
	let userCart = await User.findById(userId).then((user, error) => {
		if(error){
			return "error";
		} 
		else if(user == null){
			return null;
		}else {
			let items = user.cart;
			let arr = 0; 
			let count = 0;
			for(let i = 0; i < items.length; i++){
				if(items[i].productId == productId){
					count = count + 1;
					break;		
				}
				arr = arr + 1;

			}
			if(arr > -1){
				items.splice(arr, 1)
			}
			if(count ==  0){
				return "empty";
			}
			return user.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return result.cart;
				}
			});	
		}
	});
	console.log(userCart);

	if(userCart == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	// else if(userCart == false){
	// 	message = "User cart is empty.";
	// 	return message;
	// }
	else if(userCart == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(userCart == "empty"){
		message = "Product is not in the cart.";
		return {error: message};
	}

////////////////////////////////////////////////////////////////////////////////
	// Compute Total Of Cart
	let totalOfCart = userCart.map((items) => {
		return items.subTotal;
	});

	let totalOfCart2 = 0;
	for(let i = 0; i < totalOfCart.length; i++){
		totalOfCart2 = totalOfCart2 + totalOfCart[i];
	}

	// Update Total Cart Amount
	let isUserCartTotalUpdated = await User.findById(userId).then((result, error) => {
		if(error){
			return "error";
		} 
		else if(result == null){
			return null;
		}else {
			
			result.totalPriceOfCart = totalOfCart2;

			return result.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		}	
	});

	if(isUserCartTotalUpdated == "error"){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == false){
		message = "There is an error with the database.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == null){
		message = "User does not exists.";
		return {error: message};
	}
	else if(isUserCartTotalUpdated == true){
		
////////////////////////////////////////////////////////////////////////////////
		// Checkout Product
		return newOrder.save().then((result, error) => {
			if(error){
				message = "There is an error with the database";
				return {error: message};
			} else {
				const capitalizedName = checkoutItem.productName[0].toUpperCase() + checkoutItem.productName.slice(1);
				message = `${capitalizedName} is successfully checkout.`
				return message;
			}
		});
	}
}

// Display Items In Cart
module.exports.displayCart = (userId) => {
	return User.findById(userId).then((result, error) => {
		if(error){
			message = "There is an error with the database."
			return {error: message};
		} 
		else if(result == null){
			message = "User does not exists."
			return {error: message};
		}else {
			return result.cart;
		}
	});
}

