const Product = require("../models/Product");
const bcrypt = require("bcrypt");

let message;

// Add Product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name.toLowerCase(),
		description: reqBody.description.toLowerCase(),
		category: reqBody.category.toLowerCase(),
		price: reqBody.price
	});
	return newProduct.save().then((result, error) => {
		if(error){
			message = "There is an error with the database";
			return {error: message};
		} else {
			const capitalizedName = reqBody.name[0].toUpperCase() + reqBody.name.slice(1);
			message = `${capitalizedName} is added to the shop.`
			return {success: message};
		}
	});
}

// Get All Products
module.exports.displayAllProducts = () => {
	return Product.find().then(result => {
		if(result == null || result.length == 0){
			message = "There are no products yet. Please add one.";
			return {error: message};
		} else {
			return result;
		}
	});
}

// Get All Active Products
module.exports.displayAllActiveProducts = () => {
	return Product.find({isAvailable: true}).then(result => {
		if(result == null || result.length == 0){
			message = "There are no active products yet. Please add one.";
			return {error: message};
		} else {
			return result;
		}
	});
} 

// Get All Products By Category
module.exports.displayProductsByCategory = (reqBody) => {
	return Product.find({
		category: reqBody.category.toLowerCase(),
		isAvailable: true
	}).then(result => {
		if(result == null || result.length == 0){
			message = "There are no active products for this category yet. Please add one.";
			return {error: message};
		} else {
			return result;
		}
	});
} 

// Get All Inactive Products
module.exports.displayAllInactiveProducts = () => {
	return Product.find({isAvailable: false}).then(result => {
		if(result == null || result.length == 0){
			message = "There are no inactive products yet.";
			return {error: message};
		} else {
			return result;
		}
	});
} 

// Get Specific Products For Users
module.exports.getProductForUser = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error){
			message = "There is an error with the database"
			return {error: message};
		} 
		else if(result == null){
			message = "Product does not exists."
			return {error: message};
		}
		else if(result.isAvailable == false){
			message = "Product is not available."
			return {error: message};
		}else {
			let product = {};
			product.id = result._id;
			product.name = result.name;
			product.description = result.description;
			product.category = result.category;
			product.price = result.price;
			product.postedOn = result.createdOn;
			return product;
		}
	});
} 

// Get Specific Products for Admins
module.exports.getProductForAdmin = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error){
			message = "There is an error with the database"
			return {error: message};
		} 
		else if(result == null){
			message = "Product does not exists."
			return {error: message};
		}
		else {
			return result;
		}
	});
} 

// Update Product
module.exports.updateProduct = (productId, reqBody) => {

	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(productId, updateProduct).then((result, error) => {
		if(error){
			message = "Update failed. There is an error with the database.";
			return {error: message};
		}
		else if(result == null){
			message = "Product does not exists."
			return {error: message};
		} else {
			message = "Product is successfully updated.";
			return {success: message};
		}
	});
} 

// Archive Product
module.exports.archiveProduct = (productId, reqBody) => {

	let updateStatus = {
		isAvailable: reqBody.isAvailable
	}

	return Product.findByIdAndUpdate(productId, updateStatus).then((result, error) => {
		if(error){
			message = "Update failed. There is an error with the database.";
			return {error: message};
		}
		else if(result == null){
			message = "Product does not exists."
			return {error: message};
		} else {
			message = "Product is archived.";
			return {success: message};
		}
	});
} 