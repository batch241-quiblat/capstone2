const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const dashboardRoute = require("./routes/dashboardRoute");



const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.set("strictQuery", false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.qift0pp.mongodb.net/E-commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));


app.use("/", dashboardRoute);
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

app.listen(port, () => console.log(`The server is now running to port ${port}`));