const express = require("express");

const productController = require("../controllers/productController");
const auth = require("../middlewares/auth");
const check = require("../middlewares/productInputs");
const router = express.Router();

// Add Product
router.post("/addProduct",check.product,auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can add products."});
	}
});

// Get All Products
router.get("/all",auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.displayAllProducts().then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can display all products."});
	}
});

// Get All Active Products
router.get("/allActive", (req, res) => {
	
	productController.displayAllActiveProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Get Products By Category
router.get("/category",check.category, (req, res) => {
	
	productController.displayProductsByCategory(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Get All Inactive Products
router.get("/allInactive", auth.verify,(req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.displayAllInactiveProducts(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can display all inactive products."});
	}
});

// Get Specific Product For Users
router.get("/:id", (req, res) => {
	productController.getProductForUser(req.params.id).then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

// Get Specific Product For Admins
router.get("/admin/:id", auth.verify, (req, res) => {
		
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.getProductForAdmin(req.params.id).then(resultFromController => {
				res.send(resultFromController);
			}
		);
	} else {
		res.send({error: "Only admin can display all inactive products."});
	}
});

// Update Product
router.put("/updateProduct/:id",check.product, auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.updateProduct(req.params.id, req.body).then(resultFromController => {
				res.send(resultFromController);
			}
		);
	} else {
		res.send({error: "Only admin can update a product."});
	}
});

// Archive Product
router.patch("/archiveProduct/:id", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.archiveProduct(req.params.id, req.body).then(resultFromController => {
				res.send(resultFromController);
			}
		);
	} else {
		res.send({error: "Only admin can archive a product."});
	}
});

module.exports = router;