const express = require("express");

const userController = require("../controllers/userController");
const auth = require("../middlewares/auth");
const check = require("../middlewares/userInputs");

const router = express.Router();

// Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// User Registration
router.post("/register",check.registration, (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Login
router.post("/login",check.login, (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Profile
router.get("/profile",auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	userController.userProfile(userData.id).then(resultFromController => res.send(resultFromController));
});

// Update Profile
router.put("/updateProfile",check.profileUpdate,auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	userController.updateProfile(userData.id, req.body).then(resultFromController => res.send(resultFromController));
});

// Change Password
router.patch("/changePassword",check.changePassword,auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	userController.updatePassword(userData.id, req.body).then(resultFromController => res.send(resultFromController));
});

// Get All Users
router.get("/all",auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.displayAllUsers().then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can display all user details."});
	}
});

// Get All Items In Cart
router.get("/myCart",auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		userController.displayCart(userData.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin do not have cart."});
	}
});

// Get Specific User
router.get("/:id",auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.displayUser(req.params.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can access user details."});
	}
});

// Set User As Admin
router.patch("/setAdmin/:id",auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.setUserAsAdmin(req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can perform this method."});
	}
});

// Add To Cart
router.post("/addToCart/:id",check.addToCart,auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		userController.addToCart(userData.id, req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin cannot perform add to cart function."});
	}
});

// Add To Cart
router.patch("/updateQuantity/:id",check.quantity, auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		userController.updateQuantity(userData.id, req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin do not have cart."});
	}
});

// Remove Product From Cart 
router.patch("/removeProduct/:id", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		userController.removeProduct(userData.id, req.params.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin do not have cart."});
	}
});

// Checkout Product From Cart 
router.patch("/checkout/:id", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		userController.checkout(userData.id, req.params.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin do not have cart."});
	}
});


module.exports = router;