const express = require("express");

const orderController = require("../controllers/orderController");
const auth = require("../middlewares/auth");
const check = require("../middlewares/userInputs");

const router = express.Router();

// Add Order
router.post("/addOrder/:id",check.order, auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		orderController.createOrder(userData.id,req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin can not order."});
	}
});

// Retrieve All My Orders
router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		orderController.getMyOrders(userData.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Admin can not perform this task."});
	}
});

// Retrieve All Orders
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderController.getAllOrders().then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can display all orders."});
	}
});

// Retrieve Specific Orders
router.get("/:id", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderController.getUserOrders(req.params.id).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send({error: "Only admin can display specific user's orders"});
	}
});

module.exports = router;