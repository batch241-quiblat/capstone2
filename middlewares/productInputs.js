const hasNumber = /[0-9]/;
const hasSpecialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/;
const hasLetters = /[a-zA-Z]/;

module.exports.product = (req, res, next) => {

	if(req.body.name == "" || req.body.name == null){
		message = "Please enter the product's name.";
		return res.send({error: message});
	} 
	else if(req.body.description == "" || req.body.description == null){
		message = "Please enter the product's description.";
		return res.send({error: message});
	} 
	else if(req.body.category == "" || req.body.category == null){
		message = "Please enter the product's category.";
		return res.send({error: message});
	}
	else if((req.body.category).toLowerCase() != "consumer electronics" && (req.body.category).toLowerCase() != "clothing" && (req.body.category).toLowerCase() != "pet care" && (req.body.category).toLowerCase() != "health and wellness" && (req.body.category).toLowerCase() != "food and beverage"){
		message = "Categories allowed are Consumer Electronics, Clothing, Pet Care, Health and Wellness, and food and beverage";
		return res.send({error: message});
	}
	else if(req.body.price == "" || req.body.price == null){
		message = "Please enter the product's price.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.price) || hasSpecialCharacters.test(req.body.price)){
		message = "Price should not contain letters or special characters.";
		return res.send({error: message});
	}
	else if(req.body.price[0] == 0){
		message = "Price should not start with 0.";
		return res.send({error: message});
	} else {
		next();
	}
}

module.exports.category = (req, res, next) => {

	if(req.body.category == "" || req.body.category == null){
		message = "Please enter the product's category.";
		return res.send({error: message});
	}
	else if((req.body.category).toLowerCase() != "consumer electronics" && (req.body.category).toLowerCase() != "clothing" && (req.body.category).toLowerCase() != "pet care" && (req.body.category).toLowerCase() != "health and wellness" && (req.body.category).toLowerCase() != "food and beverage"){
		message = "Categories allowed are Consumer Electronics, Clothing, Pet Care, Health and Wellness, and food and beverage";
		return res.send({error: message});
	} else {
		next();
	}
}


	
