const jwt = require("jsonwebtoken");

const secret = "E-commerceAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// jwt.sign(data/paylod, secretKey, options)
	return jwt.sign(data, secret, { expiresIn: '1h'});
}


// Token Verification

module.exports.verify = (req, res, next) => {
	console.log(req.headers);
	
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"});
			} else {

				next();
			}
		});
	} else{
		return res.send({auth: "failed"});
	}
}

// Token Decryption
module.exports.decode = (token) => {
	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error,data) => {
			if(error){
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		});
	} else {
		return null;
	}
}