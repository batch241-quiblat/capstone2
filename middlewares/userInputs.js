const hasNumber = /[0-9]/;
const hasSpecialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
const hasUppercase = /[A-Z]/;
const hasLetters = /[a-zA-Z]/;
const emailValidation = /[a-zA-Z0-9_\-\.]+[@][a-z]+[\.][a-z][a-z]/;

let message;

module.exports.registration = (req, res, next) => {

	if(req.body.firstName == "" || req.body.firstName == null){
		message = "Please enter your first name.";
		
		return res.send({error: message});
	} 
	else if(hasNumber.test(req.body.firstName)){
		message = "First name should not contain numbers.";
		return res.send({error: message});
	} 
	else if(hasSpecialCharacters.test(req.body.firstName)){
		message = "First name should not contain special characters.";
		return res.send({error: message});
	}
	else if(req.body.lastName == "" || req.body.lastName == null){
		message = "Please enter your last name.";
		return res.send({error: message});
	} 
	else if(hasNumber.test(req.body.lastName)){
		message = "Last name should not contain numbers.";
		return res.send({error: message});
	}
	else if(hasSpecialCharacters.test(req.body.lastName)){
		message = "Last name should not contain special characters.";
		return res.send({error: message});
	}
	else if(req.body.mobileNo == "" || req.body.mobileNo == null){
		message = "Please enter your mobile number.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.mobileNo) || hasSpecialCharacters.test(req.body.mobileNo)){
		message = "Mobile number should not contain letters or special characters.";
		return res.send({error: message});
	}
	else if((req.body.mobileNo).length !== 11){
		message = "Mobile number should only have 11 digits.";
		return res.send({error: message});
	}
	else if(req.body.mobileNo[0] != 0 || req.body.mobileNo[1] != 9){
		message = "Mobile number should start with 09.";
		return res.send({error: message});
	}
	else if(req.body.email == "" || req.body.email == null){
		message = "Please enter your email address.";
		return res.send({error: message});
	}
	else if(!(emailValidation.test(req.body.email))){
		message = "Email address is not valid.";
		return res.send({error: message});
	}
	else if(req.body.password == "" || req.body.password == null){
		message = "Please enter your password";
		return res.send({error: message});
	}
	else if(req.body.password.length < 8){
		message = "Password should be at least 8 characters long.";
		return res.send({error: message});
	}
	else if(!(hasUppercase.test(req.body.password))){
		message = "Password should contain at least 1 Uppercase.";
		return res.send({error: message});
	}
	else if(!(hasNumber.test(req.body.password))){
		message = "Password should contain at least 1 number.";
		return res.send({error: message});
	}
	else if(!(hasSpecialCharacters.test(req.body.password))){
		message = "Password should contain at least 1 special character.";
		return res.send({error: message});
	}
	else {
		next();
	}
}

module.exports.login = (req, res, next) => {

	if(req.body.email == "" || req.body.email == null){
		message = "Please enter your email address.";
		return res.send({error: message});
	}
	else if(!(emailValidation.test(req.body.email))){
		message = "Please Enter a valid email address.";
		return res.send({error: message});
	}
	else if(req.body.password == "" || req.body.password == null){
		message = "Please enter your password.";
		return res.send({error: message});
	}
	else if(req.body.password.length < 8){
		message = "Password should be at least 8 characters long.";
		return res.send({error: message});
	}else {
		next();
	}
}

module.exports.profileUpdate = (req, res, next) => {

	if(req.body.firstName == "" || req.body.firstName == null){
		message = "Please enter your first name.";
		return res.send({error: message});
	} 
	else if(hasNumber.test(req.body.firstName)){
		message = "First name should not contain numbers.";
		return res.send({error: message});
	} 
	else if(hasSpecialCharacters.test(req.body.firstName)){
		message = "First name should not contain special characters.";
		return res.send({error: message});
	}
	else if(req.body.lastName == "" || req.body.lastName == null){
		message = "Please enter your last name.";
		return res.send({error: message});
	} 
	else if(hasNumber.test(req.body.lastName)){
		message = "Last name should not contain numbers.";
		return res.send({error: message});
	}
	else if(hasSpecialCharacters.test(req.body.lastName)){
		message = "Last name should not contain special characters.";
		return res.send({error: message});
	}
	else if(req.body.mobileNo == "" || req.body.mobileNo == null){
		message = "Please enter your mobile number.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.mobileNo) || hasSpecialCharacters.test(req.body.mobileNo)){
		message = "Mobile number should not contain letters or special characters.";
		return res.send({error: message});
	}
	else if((req.body.mobileNo).length !== 11){
		message = "Mobile number should have 11 digits.";
		return res.send({error: message});
	}
	else if(req.body.mobileNo[0] != 0 || req.body.mobileNo[1] != 9){
		message = "Mobile number should start with 09.";
		return res.send({error: message});
	}else {
		next();
	}
}

module.exports.changePassword = (req, res, next) => {

	if(req.body.newPassword == "" || req.body.newPassword == null){
		message = "Please enter your new password";
		return res.send({error: message});
	}
	else if(req.body.newPassword.length < 8){
		message = "New password should be at least 8 characters long.";
		return res.send({error: message});
	}
	else if(!(hasUppercase.test(req.body.newPassword))){
		message = "New password should contain at least 1 Uppercase.";
		return res.send({error: message});
	}
	else if(!(hasNumber.test(req.body.newPassword))){
		message = "New password should contain at least 1 number.";
		return res.send({error: message});
	}
	else if(!(hasSpecialCharacters.test(req.body.newPassword))){
		message = "New password should contain at least 1 special character.";
		return res.send({error: message});
	}
	else {
		next();
	}
}

module.exports.addToCart = (req, res, next) => {

	if(req.body.quantity == "" || req.body.quantity == null){
		message = "Please enter product quantity.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.quantity) || hasSpecialCharacters.test(req.body.quantity)){
		message = "Quantity should not contain letters or special characters";
		return res.send({error: message});
	}
	else {
		next();
	}
}

module.exports.quantity = (req, res, next) => {

	if(req.body.quantity == "" || req.body.quantity == null){
		message = "Please enter product quantity.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.quantity) || hasSpecialCharacters.test(req.body.quantity)){
		message = "Quantity should not contain letters or special characters.";
		return res.send({error: message});
	}
	else {
		next();
	}
}

module.exports.order = (req, res, next) => {

	if(req.body.quantity == "" || req.body.quantity == null){
		message = "Please enter product quantity.";
		return res.send({error: message});
	}
	else if(hasLetters.test(req.body.quantity) || hasSpecialCharacters.test(req.body.quantity)){
		message = "Quantity should not contain letters or special characters.";
		return res.send({error: message});
	}
	else {
		next();
	}
}